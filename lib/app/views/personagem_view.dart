import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/tabuleiro_view.dart';

class PersonagemView extends StatefulWidget {
  final Game game;

  const PersonagemView({Key key, this.game}) : super(key: key);

  @override
  _PersonagemViewState createState() => _PersonagemViewState();
}

class _PersonagemViewState extends State<PersonagemView> {
  Widget _botaoIniciarViagem = BotaoIniciarViagemDesabilitado();

  int numberOfPlayers = 0;

  final Map chooseimageList = {
    0: {"selecionado": false, "imagem": Image.asset("assets/images/personagem.png")},
    1: {"selecionado": false, "imagem": Image.asset("assets/images/personagem.png")},
    2: {"selecionado": false, "imagem": Image.asset("assets/images/personagem.png")},
    3: {"selecionado": false, "imagem": Image.asset("assets/images/personagem.png")},
    4: {"selecionado": false, "imagem": Image.asset("assets/images/personagem.png")},
    5: {"selecionado": false, "imagem": Image.asset("assets/images/personagem.png")},
    6: {"selecionado": false, "imagem": Image.asset("assets/images/personagem.png")},
    7: {"selecionado": false, "imagem": Image.asset("assets/images/personagem.png")},
  };

  final Map chosenimageList = {
    0: {"imagem": Image.asset("assets/images/container_adicionar_personagem.png")},
    1: {"imagem": Image.asset("assets/images/container_adicionar_personagem.png")},
    2: {"imagem": Image.asset("assets/images/container_adicionar_personagem.png")},
    3: {"imagem": Image.asset("assets/images/container_adicionar_personagem.png")},
    4: {"imagem": Image.asset("assets/images/container_adicionar_personagem.png")},
    5: {"imagem": Image.asset("assets/images/container_adicionar_personagem.png")}
  };

  @override
  Widget build(BuildContext context) {
    widget.game.debug("PERSONAGEM");
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Image.asset("assets/images/titulo_personagem.png"),
      ),
      body: Stack(children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Image.asset("assets/images/card_cenario.png", fit: BoxFit.fill),
        ),
        Padding(
          padding: const EdgeInsets.fromLTRB(0, 100, 0, 0),
          child: Column(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
              children: [
            Container(
              height: 200,
              child: Padding(
                padding: const EdgeInsets.fromLTRB(16, 0, 16, 0),
                child: ListView.separated(
                  shrinkWrap: true,
                  itemCount: widget.game.playerListSelect.length,
                  scrollDirection: Axis.horizontal,
                  separatorBuilder: (BuildContext context, int index) => Divider(indent: 10),
                  itemBuilder: (BuildContext context, int index) {
                    return Column(
                      children: [
                        Container(
                          height: 150,
                          child: InkWell(
                            onTap: () {
                              setState(() {
                                if (widget.game.playerListSelect[index].selected) {
                                  widget.game.playerListSelect[index].selected = false;
                                  numberOfPlayers -= 1;
                                  chosenimageList[numberOfPlayers]["imagem"] = Image.asset("assets/images/container_adicionar_personagem.png");
                                } else if (numberOfPlayers < 6) {
                                  widget.game.playerListSelect[index].selected = true;
                                  chosenimageList[numberOfPlayers]["imagem"] = Image.asset("assets/images/" + widget.game.playerListSelect[index].imagemMini + ".png");
                                  numberOfPlayers += 1;
                                }
                                if (numberOfPlayers > 1) {
                                  widget.game.numberOfPlayers = numberOfPlayers;
                                  _botaoIniciarViagem = BotaoIniciarViagem(widget.game);
                                } else {
                                  _botaoIniciarViagem = BotaoIniciarViagemDesabilitado();
                                }
                              });
                            },
                            child: widget.game.playerListSelect[index].selected
                                ? Image.asset("assets/images/" + widget.game.playerListSelect[index].imagemSelected + ".png")
                                : Image.asset("assets/images/" + widget.game.playerListSelect[index].imagem + ".png"),
                          ),
                        ),
                        Text(widget.game.playerListSelect[index].name)
                      ],
                    );
                  },
                ),
              ),
            ),
            Padding(
              padding: const EdgeInsets.fromLTRB(100, 0, 100, 0),
              child: Stack(
                children: [
                  SizedBox(
                    height: 90,
                    child: Image.asset("assets/images/container_personagem_selecionado.png"),
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(
                        height: 90,
                        child: ListView.separated(
                          shrinkWrap: true,
                          itemCount: 6,
                          scrollDirection: Axis.horizontal,
                          separatorBuilder: (BuildContext context, int index) => Divider(indent: 10),
                          itemBuilder: (BuildContext context, int index) {
                            return Center(child: chosenimageList[index]["imagem"]);
                          },
                        ),
                      ),
                      SizedBox(width: 20),
                      _botaoIniciarViagem,
                    ],
                  ),
                ],
              ),
            ),
          ]),
        ),
      ]),
    );
  }
}

class BotaoIniciarViagem extends StatelessWidget {
  final Game game;

  BotaoIniciarViagem(this.game);

  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {
          game.fetchPlayersForGame();
          Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => TabuleiroView(game: game)),
          );
        },
        child: Image.asset(
          "assets/images/botao_iniciar_viagem.png",
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

class BotaoIniciarViagemDesabilitado extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Material(
      color: Colors.transparent,
      child: InkWell(
        onTap: () {},
        child: Image.asset(
          "assets/images/botao_iniciar_viagem_desabilitado.png",
          fit: BoxFit.cover,
        ),
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/tabuleiro_view.dart';

class BauVazioView extends StatefulWidget {
  final Game game;

  const BauVazioView({Key key, this.game}) : super(key: key);

  @override
  _BauVazioViewState createState() => _BauVazioViewState();
}

class _BauVazioViewState extends State<BauVazioView> {
  Widget build(BuildContext context) {
    print("====================================");
    print("Bau Vazio");
    print("numberOfRounds: " + widget.game.numberOfRounds.toString());
    print("numberOfPlayers: " + widget.game.numberOfPlayers.toString());
    print("currentPlayer: " + widget.game.currentPlayer.toString());
    print("currentRoundPhase: " + widget.game.currentRoundPhase.toString());
    print("currentRound: " + widget.game.currentRound.toString());
    print("====================================");
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.blue.withOpacity(0.3),
          ),
          Container(child: Image.asset("assets/images/modal_bau.png", height: 350), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/bau_vazio.png", height: 150),
                SizedBox(height: 10),
                Text("Infelizmente o baú estava vazio!"),
                SizedBox(height: 10),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      widget.game.nextMove();
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => TabuleiroView(game: widget.game)));
                    },
                    child: Image.asset(
                      "assets/images/botao_passar_vez.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

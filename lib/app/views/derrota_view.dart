import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/home_view.dart';

class DerrotaView extends StatefulWidget {
  final Game game;

  const DerrotaView({Key key, this.game}) : super(key: key);

  @override
  _DerrotaViewState createState() => _DerrotaViewState();
}

class _DerrotaViewState extends State<DerrotaView> {
  Widget build(BuildContext context) {
    widget.game.debug("Derrota");
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.blue.withOpacity(0.3),
          ),
          Container(child: Image.asset("assets/images/modal_derrota.png", height: 350), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/imagem_derrota.png"),
                SizedBox(height: 20),
                Container(
                    width: 200, 
                    child: RichText(
                      text: TextSpan(
                        text: "Infelizmente vocês ",
                        style: Theme.of(context).textTheme.subtitle2,
                        children: [
                          TextSpan(text: "não tiveram o poder necessário ", style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.green)),
                          TextSpan(text: "para vencer!", style: Theme.of(context).textTheme.subtitle2),
                        ],
                      ),
                    ),
                ),
                SizedBox(height: 20),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => HomeView()),
                      );
                    },
                    child: Image.asset(
                      "assets/images/botao_voltar_menu.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

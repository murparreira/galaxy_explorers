import 'package:bordered_text/bordered_text.dart';
import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/derrota_view.dart';
import 'package:galaxy_explorers/app/views/desafio_view.dart';
import 'package:galaxy_explorers/app/views/home_view.dart';
import 'package:galaxy_explorers/app/views/local_view.dart';
import 'package:galaxy_explorers/app/views/sair_view.dart';
import 'package:galaxy_explorers/app/views/vitoria_view.dart';
import 'package:galaxy_explorers/app/views/eliminado_view.dart';

class TabuleiroView extends StatefulWidget {
  final Game game;

  const TabuleiroView({Key key, this.game}) : super(key: key);

  @override
  _TabuleiroViewState createState() => _TabuleiroViewState();
}

class _TabuleiroViewState extends State<TabuleiroView> {

  void initState() {
    super.initState();
    WidgetsBinding.instance.addPostFrameCallback((_) => checkGameIsOver());
  }

  @override
  Widget build(BuildContext context) {
    widget.game.debug("TABULEIRO");
    return WillPopScope(
      child: Scaffold(
        body: Stack(
          children: [
            Container(
              width: MediaQuery.of(context).size.width,
              height: MediaQuery.of(context).size.height,
              child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
            ),
            Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Padding(
                padding: const EdgeInsets.all(32.0),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: [
                    Column(
                      children: [
                        Image.asset("assets/images/rodada_" + widget.game.currentRound.toString() + ".png"),
                        SizedBox(height: 20),
                        widget.game.isPlayerInLifeRisk() ?
                        Column(children: [
                          Image.asset("assets/images/risco_vida.png"),
                          for(var player in widget.game.playersAtRisk()) Text(player.name + " - " + player.locationOfLifeRisk)
                        ]) :
                        Container(),
                      ]
                    ),
                    Column(
                      children: [
                        BorderedText(
                          strokeWidth: 2.0,
                          child: Text("Rodada " + widget.game.currentRound.toString(),
                            style: TextStyle(
                              fontFamily: "Goldman", fontSize: 24,
                              decoration: TextDecoration.none,
                              decorationColor: Colors.black,
                            ),
                          ),
                        ),
                        BorderedText(
                          strokeWidth: 2.0,
                          child: Text(widget.game.currentPlayer.name,
                            style: TextStyle(
                              fontFamily: "Goldman",
                              decoration: TextDecoration.none,
                              decorationColor: Colors.black,
                            ),
                          ),
                        ),
                        SizedBox(height: 10),
                        Image.asset("assets/images/" + widget.game.currentPlayer.imagemMini + ".png")
                      ],
                    ),
                    Column(
                      children: [
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () => {},
                            child: Image.asset("assets/images/botao_configuracoes.png"),
                          ),
                        ),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () => {},
                            child: Image.asset("assets/images/botao_ajuda.png"),
                          ),
                        ),
                        Material(
                          color: Colors.transparent,
                          child: InkWell(
                            onTap: () {
                              Navigator.of(context).push(
                                MaterialPageRoute(builder: (context) => SairView(game: widget.game)),
                              );
                            },
                            child: Image.asset("assets/images/botao_abandonar_jogo.png"),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              widget.game.currentRoundPhase == "local"
                  ? Container(
                      height: 140,
                      child: ListView.separated(
                        itemCount: widget.game.locationList.length,
                        scrollDirection: Axis.horizontal,
                        separatorBuilder: (BuildContext context, int index) => Divider(indent: 10),
                        itemBuilder: (BuildContext context, int index) {
                          return LocalWidget(widget.game, widget.game.locationList[index]);
                        },
                      ),
                    )
                  : new Container(),
              Padding(
                padding: const EdgeInsets.fromLTRB(0, 0, 0, 16),
                child: Align(
                  alignment: Alignment.bottomCenter,
                  child: Container(child: _buildBottomOfTabuleiro()),
                ),
              ),
            ]),
          ],
        ),
      ),
      onWillPop: () => showDialog(
        context: context,
        builder: (c) => AlertDialog(
          title: Text("Aviso!"),
          content: Text("Deseja sair da partida?"),
          actions: [
            FlatButton(
                child: Text('Yes'),
                onPressed: () {
                  Navigator.of(context).popUntil((route) => route.isFirst);
                  Navigator.of(context).pushReplacement(
                    MaterialPageRoute(builder: (context) => HomeView()),
                  );
                }),
            FlatButton(
              child: Text('No'),
              onPressed: () => Navigator.pop(c, false),
            ),
          ],
        ),
      ),
    );
  }

  checkGameIsOver() {
    if (widget.game.currentPlayer.lifeRisk && widget.game.currentRoundPhase == "tabuleiro") {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => EliminadoView(game: widget.game)));
    }
    if (widget.game.numberOfPlayers == 0) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DerrotaView(game: widget.game)));
    }
    if (widget.game.checkGameIsOver()) {
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => VitoriaView(game: widget.game)));
    }
  }

  _buildBottomOfTabuleiro() {
    switch (widget.game.currentRoundPhase) {
      case "tabuleiro":
        return Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () => Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => DesafioView(game: widget.game))),
            child: Image.asset("assets/images/botao_selecionar_desafio.png"),
          ),
        );
        break;
      case "local":
        return BorderedText(
          strokeWidth: 2.0,
          child: Text("PARA ONDE VOCÊ IRÁ?",
            style: TextStyle(
              fontFamily: "Goldman", fontSize: 30,
              decoration: TextDecoration.none,
              decorationColor: Colors.black,
            ),
          ),
        );
        break;
      case "passar_vez":
        return Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              widget.game.nextMove();
              Navigator.push(context, MaterialPageRoute(builder: (context) => TabuleiroView(game: widget.game)));
            },
            child: Image.asset("assets/images/botao_passar_vez.png"),
          ),
        );
        break;
    }
  }

}

class LocalWidget extends StatelessWidget {
  Game game;
  Location location;

  LocalWidget(this.game, this.location);

  @override
  Widget build(BuildContext context) {
    var borderColor;
    if (location.hasLifeRisk) {
      borderColor = Colors.red;
    } else {
      if (location.areAllMonstersDefeated()) {
        borderColor = Colors.green;
      } else {
        borderColor = Colors.blueGrey;
      }
    }
    return Column(children: [
      InkWell(
        child: Container(
          padding: EdgeInsets.all(5),
          decoration: BoxDecoration(
             borderRadius: BorderRadius.circular(25.0),
              color: borderColor,
              border: Border.all(color: borderColor, width: 5)
          ),
          child: Image.asset("assets/images/" + location.imagem + ".png", width: 90, height: 90),
        ),
        onTap: () {
          showDialog(
              context: context,
              builder: (BuildContext context) {
                return Stack(alignment: Alignment.center, children: [
                  Image.asset("assets/images/modal_desafio_finalizado.png"),
                  Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    crossAxisAlignment: CrossAxisAlignment.center,
                    children: [
                      SizedBox(height: 10),
                      Container(width: 250, child: Text("Entrar em " + location.name, textAlign: TextAlign.center, style: Theme.of(context).textTheme.headline6.copyWith(fontFamily: "Goldman", fontSize: 16))),
                      SizedBox(height: 20),
                      Material(
                        child: InkWell(
                            onTap: () {
                              Navigator.pop(context);
                              Navigator.of(context).push(
                                MaterialPageRoute(builder: (context) => LocalView(game: game, location: location)),
                              );
                            },
                            child: Image.asset("assets/images/botao_entendi.png")),
                      ),
                    ],
                  ),
                ]);
              });
        },
      ),
      BorderedText(
        strokeWidth: 2.0,
        child: Text(location.name,
          style: TextStyle(
            fontFamily: "Goldman",
            decoration: TextDecoration.none,
            decorationColor: Colors.black,
          ),
        ),
      ),
    ]);
  }
}

import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/tabuleiro_view.dart';

class RecompensaView extends StatefulWidget {
  final Game game;

  const RecompensaView({Key key, this.game}) : super(key: key);

  @override
  _RecompensaViewState createState() => _RecompensaViewState();
}

class _RecompensaViewState extends State<RecompensaView> {
  Widget build(BuildContext context) {
    widget.game.debug("Recompensa");
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.blue.withOpacity(0.3),
          ),
          Container(child: Image.asset("assets/images/modal_recompensa.png", height: 350), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/espada.png", height: 150),
                SizedBox(height: 10),
                Text("Você encontrou:"),
                Text("ESPADA GALÁTICA II", style: TextStyle(fontSize: 20, color: Colors.orange)),
                SizedBox(height: 10),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      widget.game.incrementNumberOfRewards();
                      widget.game.nextMove();
                      Navigator.of(context).push(MaterialPageRoute(builder: (context) => TabuleiroView(game: widget.game)));
                    },
                    child: Image.asset(
                      "assets/images/botao_guardar_item_passar_vez.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

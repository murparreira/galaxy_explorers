import 'dart:math';

import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/bau_vazio_view.dart';
import 'package:galaxy_explorers/app/views/recompensa_view.dart';
import 'package:galaxy_explorers/app/views/tabuleiro_view.dart';

class BauView extends StatefulWidget {
  final Game game;

  const BauView({Key key, this.game}) : super(key: key);

  @override
  _BauViewState createState() => _BauViewState();
}

class _BauViewState extends State<BauView> {
  Widget build(BuildContext context) {
    print("====================================");
    print("Bau");
    print("numberOfRounds: " + widget.game.numberOfRounds.toString());
    print("numberOfPlayers: " + widget.game.numberOfPlayers.toString());
    print("currentPlayer: " + widget.game.currentPlayer.toString());
    print("currentRoundPhase: " + widget.game.currentRoundPhase.toString());
    print("currentRound: " + widget.game.currentRound.toString());
    print("====================================");
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.blue.withOpacity(0.3),
          ),
          Container(child: Image.asset("assets/images/modal_bau.png", height: 350), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/bau.png", height: 150),
                SizedBox(height: 10),
                Text("O que tem dentro do baú?"),
                SizedBox(height: 10),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      Random random = new Random();
                      int randomNumberChance = random.nextInt(100);
                      if (randomNumberChance >= 40) {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => BauVazioView(game: widget.game)));
                      } else {
                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => RecompensaView(game: widget.game)));
                      }
                    },
                    child: Image.asset(
                      "assets/images/botao_abrir_bau.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

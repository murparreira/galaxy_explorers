import 'dart:math';

import 'package:circular_countdown_timer/circular_countdown_timer.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/tabuleiro_view.dart';

class DesafioView extends StatefulWidget {
  final Game game;

  const DesafioView({Key key, this.game}) : super(key: key);

  @override
  _DesafioViewState createState() => _DesafioViewState();
}

class _DesafioViewState extends State<DesafioView> {
  String secretCode;
  String typedCode = "";

  void initState() {
    super.initState();
    Random random = new Random();
    secretCode = random.nextInt(10000).toString();
  }

  @override
  Widget build(BuildContext context) {
    widget.game.debug("DESAFIO");
    return Scaffold(
      body: Stack(children: [
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
        ),
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          color: Colors.blue.withOpacity(0.3),
        ),
        Container(child: Image.asset("assets/images/modal_desafio.png"), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
        Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: [
              Row(mainAxisAlignment: MainAxisAlignment.spaceEvenly, crossAxisAlignment: CrossAxisAlignment.center, children: [
                CircularCountDownTimer(
                  duration: 480,
                  controller: null,
                  width: 75,
                  height: 75,
                  color: Colors.orange,
                  fillColor: Colors.white,
                  backgroundColor: null,
                  strokeWidth: 2.0,
                  textStyle: TextStyle(fontSize: 16.0, color: Colors.white, fontWeight: FontWeight.bold),
                  isReverse: true,
                  isReverseAnimation: true,
                  isTimerTextShown: true,
                  onComplete: () {
                    widget.game.changeCurrentRoundPhase("passar_vez");
                    Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TabuleiroView(game: widget.game)));
                  },
                ),
                Text("Digite o código secreto: " + secretCode),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      widget.game.changeCurrentRoundPhase("passar_vez");
                      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TabuleiroView(game: widget.game)));
                    },
                    child: Image.asset("assets/images/botao_desistir.png"),
                  ),
                ),
              ]),
              Container(
                decoration: BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.only(topLeft: Radius.circular(10), topRight: Radius.circular(10), bottomLeft: Radius.circular(10), bottomRight: Radius.circular(10)),
                  boxShadow: [
                    BoxShadow(
                      color: Colors.grey.withOpacity(0.5),
                      spreadRadius: 5,
                      blurRadius: 7,
                      offset: Offset(0, 3), // changes position of shadow
                    ),
                  ],
                ),
                width: 200,
                child: Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _getButton(
                          text: '7',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "7";
                            });
                            checkCodeIsCorrect();
                          }),
                      _getButton(
                          text: '8',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "8";
                            });
                            checkCodeIsCorrect();
                          }),
                      _getButton(
                          text: '9',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "9";
                            });
                            checkCodeIsCorrect();
                          }),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _getButton(
                          text: '4',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "4";
                            });
                            checkCodeIsCorrect();
                          }),
                      _getButton(
                          text: '5',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "5";
                            });
                            checkCodeIsCorrect();
                          }),
                      _getButton(
                          text: '6',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "6";
                            });
                            checkCodeIsCorrect();
                          }),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _getButton(
                          text: '1',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "1";
                            });
                            checkCodeIsCorrect();
                          }),
                      _getButton(
                          text: '2',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "2";
                            });
                            checkCodeIsCorrect();
                          }),
                      _getButton(
                          text: '3',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "3";
                            });
                            checkCodeIsCorrect();
                          }),
                    ],
                  ),
                  Row(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      _getButton(text: 'X', onTap: () { setState(() => typedCode = ""); }, backgroundColor: Color.fromRGBO(220, 220, 220, 1)),
                      _getButton(
                          text: '0',
                          onTap: () {
                            setState(() {
                              typedCode = typedCode + "0";
                            });
                            checkCodeIsCorrect();
                          }),
                      _getButton(text: 'X', onTap: () { setState(() => typedCode = ""); }, backgroundColor: Color.fromRGBO(220, 220, 220, 1)),
                    ],
                  ),
                ]),
              ),
              SizedBox(height: 10),
              Text("Código digitado: " + typedCode),
            ],
          ),
        ),
      ]),
    );
  }

  checkCodeIsCorrect() {
    print(typedCode);
    print(secretCode);
    if (typedCode == secretCode) {
      widget.game.changeCurrentRoundPhase("local");
      Navigator.pushReplacement(context, MaterialPageRoute(builder: (context) => TabuleiroView(game: widget.game)));
    }
  }
}

Widget _getButton({String text, Function onTap, Color backgroundColor = Colors.white, Color textColor = Colors.black}) {
  return CalculatorButton(
    label: text,
    onTap: onTap,
    size: 45,
    backgroundColor: Colors.red,
    labelColor: Colors.black,
  );
}

class CalculatorButton extends StatelessWidget {
  CalculatorButton({@required this.label, @required this.onTap, @required this.size, this.backgroundColor = Colors.white, this.labelColor = Colors.black});

  final String label;
  final VoidCallback onTap;
  final double size;
  final Color backgroundColor;
  final Color labelColor;

  @override
  Widget build(BuildContext context) {
    return Padding(
        padding: EdgeInsets.all(6),
        child: Ink(
          width: size,
          height: size,
          child: InkWell(
            customBorder: RoundedRectangleBorder(
              borderRadius: BorderRadius.all(Radius.circular(size / 2)),
            ),
            onTap: onTap,
            child: Center(
                child: Text(
              label,
              style: TextStyle(fontSize: 24, color: labelColor),
            )),
          ),
        ));
  }
}

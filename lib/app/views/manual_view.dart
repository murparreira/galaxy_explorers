import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/views/cenario_view.dart';

class ManualView extends StatefulWidget {
  @override
  _ManualViewState createState() => _ManualViewState();
}

class _ManualViewState extends State<ManualView> {
  Widget build(BuildContext context) {
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/home.png", fit: BoxFit.fill),
          ),
          Container(child: Image.asset("assets/images/modal_manual.png"), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
          Padding(
            padding: const EdgeInsets.all(50.0),
            child: SingleChildScrollView(
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: [
                  Text("Manual", style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 20),
                  Text("1. Preparação", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  Text("Podem jogar de 2 a 6 jogadores. Você precisará baixar o jogo no seu smartphone ou tablet e iniciar um jogo escolhendo sua missão e seus tripulantes."),
                  SizedBox(height: 20),
                  Text("2. Distribuição de componentes", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  Text(
                      "Você precisará separar as cartas de cada jogador escolhido ao seu respectivo representante, entregue para cada jogador três marcadores de dano para que ele possa marcar sua vida na carta do seu personagem "),
                  SizedBox(height: 20),
                  Text("3. Inicio e término de jogo", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  Text(
                      "O jogador que inicia será o que assistiu um filme de viagem espacial por último. O jogo tem seu inicio com o jogador escolhendo suas ações que serão feitas no seu turno, cada jogador começa com 3 pontos de ação e também 3 pontos de vida. As ações que ele pode escolher são:"),
                  SizedBox(height: 10),
                  Text("Movimentar: O jogador gasta 1 ponto de ação e anda 2 blocos para direção que escolher."),
                  SizedBox(height: 10),
                  Text("Atacar: O jogador gasta 1 ponto de ação para atacar o monstro. O jogador escolhe o alvo que irá atacar e rola o dado de combate, caso o jogador tenha sucesso na sua rolagem, ele elimina o monstro e isso aumenta sua adrenalina fazendo com que o mesmo possa jogar o dado novamente mirando em outro alvo até eliminar todos os monstros da sala ou não ter sucesso no dado."),
                  SizedBox(height: 10),
                  Text("Obs: essa adrenalina ganhada pelo dado não se aplica a monstro chefe, nele será necessário gastar uma outra ação para tentar derrota-lo.", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("Resolver desafio: O jogador gasta 1 ponto de ação e é sorteado no aplicativo um desafio para ele resolver, caso consiga resolver ele entra na sala automaticamente sem gastar nenhuma ação extra. Caso não consiga resolver ele pode tentar novamente um outro desafio gastando mais um ponto de ação."),
                  SizedBox(height: 20),
                  Text("4. Jogadas que não utilizam ação", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("Pedir ajuda: Quando um jogador finalizar todas suas ações e se ver em meio de monstros, ele pode solicitar ajuda da sua equipe para resgata-lo."),
                  SizedBox(height: 10),
                  Text("Pesquisar recompensa: Após um árduo combate, o jogador pode procurar na sala se encontra algo valioso para sua missão."),
                  SizedBox(height: 20),
                  Text("5. Personagens", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("Tom: Líder nato. Ao fim do seu turno ele pode escolher um jogador a sua escolha e dar uma ação extra que dura até o fim da rodada."),
                  SizedBox(height: 10),
                  Text("Julia: Restaura 1 ponto de vida de algum aliado que esteja próximo a ela (mínimo 1 espaço de distancia)."),
                  SizedBox(height: 10),
                  Text("Zivia: Habilidosa exploradora, a cada rodada no seu primeiro movimento Zivia anda três blocos ao invés de dois."),
                  SizedBox(height: 10),
                  Text("Collin: Gigante poderoso, com sua força é capaz de dar 2 pontos de dano ao invés de 1 com um ataque."),
                  SizedBox(height: 10),
                  Text("Euranes: Centauro que pode carregar um jogador com ele por dois blocos ou entrar em uma sala, o jogador que pegou a ajudinha não precisa resolver o desafio caso entrem em uma sala. Ambos os jogadores precisam estar no mesmo bloco para que essa ação aconteça. "),
                  SizedBox(height: 10),
                  Text("Lucy: Vampiro que ao derrotar um monstro ele recupera 1 ponto de vida perdido."),
                  SizedBox(height: 10),
                  Text("Mauro: Humano que possui a habilidade passos leves e consegue passar entre os monstros que nascem fora das construções."),
                  SizedBox(height: 10),
                  Text("Edgar: Humano que possui a habilidade de que caso as rodadas acabem, ele permite que os jogadores joguem uma rodada extra para tentar completar o objetivo."),
                  SizedBox(height: 20),
                  Text("6. Maneiras de Perder", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("Acabar todos os monstros da caixa."),
                  SizedBox(height: 10),
                  Text("Todos os jogadores morrerem"),
                  SizedBox(height: 10),
                  Text("Acabar as rodadas"),
                  SizedBox(height: 20),
                  Text("7. Maneira de Vencer", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("Cumpra os objetivos descritos no Cenário."),
                  SizedBox(height: 20),
                  Text("8. Fases do turno do jogador", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("No inicio do seu turno você pode escolher entre fazer um movimento, resolver desafio, atacar, pedir ajuda e pesquisar recompensa. Após gastar suas ações o turno passa para o próximo jogador e assim até chegar ao ultimo jogador. Quando todos os jogadores jogam é a vez do tabuleiro."),
                  SizedBox(height: 20),
                  Text("9. Turno do tabuleiro", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("Após o turno de todos os jogadores chegou a vez do jogo! O aplicativo sorteia aleatoriamente os espaços marcados no tabuleiro com PERIGO que são os lugarem onde podem nascer monstros. Se já houver monstros no tabuleiro, cada monstro movimenta 1 bloco em direção ao jogador mais próximo."),
                  SizedBox(height: 20),
                  Text("10. Combate", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("No inicio do combate, o jogador deve jogar o dado, caso a face do dado sorteada seja espada ele dará 1 ponto de dano no monstro, os monstros que são nível 1 tem apenas 1 ponto de vida, então você mata o monstro e pode jogar o dado novamente sem gastar ação mirando em um outro alvo. Caso não tenha mais monstros você não recebe essa ação de jogar um dado como ação extra. Se não tiver êxito ao jogar o dado, você pode tentar novamente caso tenha mais ações."),
                  SizedBox(height: 10),
                  Text("Os monstros que são de nível 1 possuem 1 ponto de vida."),
                  SizedBox(height: 10),
                  Text("Os monstros nível chefe possuem 3 pontos de vida."),
                  SizedBox(height: 20),
                  Text("11. Procurando Recompensa", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("O jogador para procurar uma recompensa ele precisa de duas condições que são:"),
                  SizedBox(height: 10),
                  Text("A sala estar limpa e livre de perigos."),
                  SizedBox(height: 10),
                  Text("Ter derrotado um monstro chefe na sala que está procurando recompensa."),
                  SizedBox(height: 10),
                  Text("Após cumprir essas duas condições, o jogador pode sortear se conseguiu receber uma recompensa ou não."),
                  SizedBox(height: 20),
                  Text("12. A morte", style: Theme.of(context).textTheme.subtitle2.copyWith(fontWeight: FontWeight.bold, color: Colors.blue)),
                  SizedBox(height: 10),
                  Text("O jogador morre, quando sua vida chega a zero. Para isso acontecer ele precisa receber dano, lembrando que o jogador começa com três pontos de vida. Quando chega o turno dos monstros, caso haja algum monstro no mesmo bloco que você ele te dá um ponto de dano, se for um monstro chefe o jogador recebe dois pontos de dano. Quando o jogador morre, ele é retirado do turno dos jogadores no aplicativo automaticamente."),
                  SizedBox(height: 30),
                  Material(
                    color: Colors.transparent,
                    child: InkWell(
                      onTap: () => Navigator.of(context).push(
                        MaterialPageRoute(builder: (context) => CenarioView()),
                      ),
                      child: Image.asset(
                        "assets/images/botao_iniciar.png",
                        fit: BoxFit.cover,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }
}

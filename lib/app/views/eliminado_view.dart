import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/tabuleiro_view.dart';

class EliminadoView extends StatefulWidget {
  final Game game;

  const EliminadoView({Key key, this.game}) : super(key: key);

  @override
  _EliminadoViewState createState() => _EliminadoViewState();
}

class _EliminadoViewState extends State<EliminadoView> {
  Widget build(BuildContext context) {
    widget.game.debug("Eliminado");
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.blue.withOpacity(0.3),
          ),
          Container(child: Image.asset("assets/images/modal_eliminado.png", height: 350), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/imagem_eliminado.png"),
                SizedBox(height: 20),
                Container(
                    width: 200, 
                    child: RichText(
                      text: TextSpan(
                        text: "Infelizmente ",
                        style: Theme.of(context).textTheme.subtitle2,
                        children: [
                          TextSpan(text: "ninguém conseguiu ", style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.red)),
                          TextSpan(text: "te ajudar. Seu personagem ", style: Theme.of(context).textTheme.subtitle2),
                          TextSpan(text: "morreu ", style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.red)),
                          TextSpan(text: "e ", style: Theme.of(context).textTheme.subtitle2),
                          TextSpan(text: "foi eliminado ", style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.red)),
                          TextSpan(text: "do tabuleiro!", style: Theme.of(context).textTheme.subtitle2),
                        ],
                      ),
                    ),
                ),
                SizedBox(height: 20),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      widget.game.eliminatePlayerAndGoToNextMove("tabuleiro");
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => TabuleiroView(game: widget.game)),
                      );
                    },
                    child: Image.asset(
                      "assets/images/botao_passar_vez.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

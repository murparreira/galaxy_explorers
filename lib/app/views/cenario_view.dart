import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/personagem_view.dart';

class CenarioView extends StatefulWidget {
  @override
  _CenarioViewState createState() => _CenarioViewState();
}

class _CenarioViewState extends State<CenarioView> {
  Game game;
  List<Cenario> cenarioList;

  var dificuldade_selecionada = "facil";
  var dados_dificuldade = {
    "facil": {
      "imagem_select": Image.asset("assets/images/select_facil.png"),
      "imagem_opcao": Image.asset("assets/images/opcao_facil.png"),
      "imagem_opcao_selecionada": Image.asset("assets/images/opcao_facil_selecionado.png")
    },
    "normal": {
      "imagem_select": Image.asset("assets/images/select_normal.png"),
      "imagem_opcao": Image.asset("assets/images/opcao_normal.png"),
      "imagem_opcao_selecionada": Image.asset("assets/images/opcao_normal_selecionado.png")
    },
    "dificil": {
      "imagem_select": Image.asset("assets/images/select_dificil.png"),
      "imagem_opcao": Image.asset("assets/images/opcao_dificil.png"),
      "imagem_opcao_selecionada": Image.asset("assets/images/opcao_dificil_selecionado.png")
    }
  };

  @override
  Widget build(BuildContext context) {
    game = new Game();
    game.fetchLocations();
    game.fetchPlayerListSelect();
    game.debug("CENARIO");
    return Scaffold(
      extendBodyBehindAppBar: true,
      appBar: AppBar(
        backgroundColor: Colors.transparent,
        elevation: 0,
        centerTitle: true,
        title: Image.asset("assets/images/titulo_cenario.png"),
      ),
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/home.png", fit: BoxFit.fill),
          ),
          Padding(
            padding: const EdgeInsets.fromLTRB(0, 50, 0, 0),
            child: ListView(
              scrollDirection: Axis.horizontal,
              children: [
                CenarioWidget(dados_dificuldade, dificuldade_selecionada, game),
              ],
            ),
          ),
        ],
      ),
    );
  }
}

class CenarioWidget extends StatefulWidget {
  Game game;
  var dados_dificuldade;
  var dificuldade_selecionada;

  CenarioWidget(this.dados_dificuldade, this.dificuldade_selecionada, this.game);

  @override
  _CenarioWidgetState createState() => _CenarioWidgetState();
}

class _CenarioWidgetState extends State<CenarioWidget> {
  @override
  Widget build(BuildContext context) {
    return SizedBox(
      width: MediaQuery.of(context).size.width - 80,
      child: Padding(
        padding: const EdgeInsets.all(16.0),
        child: Card(
          elevation: 0,
          color: Colors.transparent,
          child: Container(
            decoration: BoxDecoration(
              image: DecorationImage(
                image: AssetImage("assets/images/card_cenario.png"),
                fit: BoxFit.fill,
                alignment: Alignment.topCenter,
              ),
            ),
            child: Padding(
              padding: const EdgeInsets.all(16.0),
              child: Row(
                children: [
                  Expanded(flex: 5, child: Image.asset("assets/images/cenario.png")),
                  Expanded(
                    flex: 5,
                    child: Padding(
                      padding: const EdgeInsets.all(16.0),
                      child: Scrollbar(
                        child: SingleChildScrollView(
                          child: SizedBox(
                            height: 450,
                            child: Column(
                              children: [
                                Text("Planeta Kersey", style: TextStyle(fontFamily: 'Goldman')),
                                Spacer(flex: 2),
                                Text(
                                    "Os tripulantes buscam seguir o sonho de se tornarem os maiores exploradores espaciais da galáxia. Eles acreditam em uma antiga lenda dizendo que no Planeta Kersey que fica a 1,4 bilhão de km de distancia é um lugar onde existe vários tesouros jamais sonhados. \n\nOs exploradores terão que reunir as três partes do tesouro e retornar a nave antes que o oxigênio acabe. Entretanto a exploração será mais árdua do que parece, pois além dos recursos limitados, a tripulação terá que sobreviver aos monstros que habitam esse planeta hostil.",
                                    style: TextStyle(fontFamily: 'Goldman')),
                                Spacer(flex: 2),
                                Align(alignment: Alignment.bottomLeft, child: Text("Selecione a dificuldade:", style: TextStyle(fontFamily: 'Goldman'))),
                                Spacer(flex: 2),
                                InkWell(
                                    child: widget.dados_dificuldade[widget.dificuldade_selecionada]["imagem_select"],
                                    onTap: () {
                                      showDialog(
                                          context: context,
                                          builder: (BuildContext context) {
                                            return Stack(alignment: Alignment.center, children: [
                                              Image.asset("assets/images/modal_dificuldade.png", height: 350, fit: BoxFit.cover),
                                              Column(
                                                mainAxisAlignment: MainAxisAlignment.center,
                                                crossAxisAlignment: CrossAxisAlignment.center,
                                                children: [
                                                  Text("Selecione", style: Theme.of(context).textTheme.headline6.copyWith(fontFamily: "Goldman")),
                                                  SizedBox(height: 10),
                                                  Material(
                                                    child: InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          widget.dificuldade_selecionada = "facil";
                                                          widget.game.cenario.dificuldade = "facil";
                                                        });
                                                        Navigator.pop(context, false);
                                                      },
                                                      child: _buildOpcoesFacil(),
                                                    ),
                                                  ),
                                                  SizedBox(height: 10),
                                                  Material(
                                                    child: InkWell(
                                                      onTap: () {
                                                        setState(() {
                                                          widget.dificuldade_selecionada = "normal";
                                                          widget.game.cenario.dificuldade = "normal";
                                                        });
                                                        Navigator.pop(context, false);
                                                      },
                                                      child: _buildOpcoesNormal(),
                                                    ),
                                                  ),
                                                  SizedBox(height: 10),
                                                  Material(
                                                    child: InkWell(
                                                        onTap: () {
                                                          setState(() {
                                                            widget.dificuldade_selecionada = "dificil";
                                                            widget.game.cenario.dificuldade = "dificil";
                                                          });
                                                          Navigator.pop(context, false);
                                                        },
                                                        child: _buildOpcoesDificil()),
                                                  ),
                                                ],
                                              ),
                                            ]);
                                          });
                                    }),
                                Spacer(flex: 2),
                                SizedBox(
                                  width: double.infinity,
                                  child: Material(
                                    color: Colors.transparent,
                                    child: InkWell(
                                      onTap: () {
                                        Navigator.of(context).push(MaterialPageRoute(builder: (context) => PersonagemView(game: widget.game)));
                                      },
                                      child: Image.asset(
                                        "assets/images/botao_prosseguir.png",
                                        fit: BoxFit.cover,
                                      ),
                                    ),
                                  ),
                                ),
                              ],
                            ),
                          ),
                        ),
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildOpcoesFacil() {
    if (widget.dificuldade_selecionada == "facil") {
      return widget.dados_dificuldade["facil"]["imagem_opcao_selecionada"];
    } else {
      return widget.dados_dificuldade["facil"]["imagem_opcao"];
    }
  }

  Widget _buildOpcoesNormal() {
    if (widget.dificuldade_selecionada == "normal") {
      return widget.dados_dificuldade["normal"]["imagem_opcao_selecionada"];
    } else {
      return widget.dados_dificuldade["normal"]["imagem_opcao"];
    }
  }

  Widget _buildOpcoesDificil() {
    if (widget.dificuldade_selecionada == "dificil") {
      return widget.dados_dificuldade["dificil"]["imagem_opcao_selecionada"];
    } else {
      return widget.dados_dificuldade["dificil"]["imagem_opcao"];
    }
  }
}

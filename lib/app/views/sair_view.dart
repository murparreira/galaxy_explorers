import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/home_view.dart';

class SairView extends StatefulWidget {
  final Game game;

  const SairView({Key key, this.game}) : super(key: key);

  @override
  _SairViewState createState() => _SairViewState();
}

class _SairViewState extends State<SairView> {
  Widget build(BuildContext context) {
    widget.game.debug("Sair");
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.blue.withOpacity(0.3),
          ),
          Container(child: Image.asset("assets/images/modal_sair.png"), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/imagem_sair.png"),
                SizedBox(height: 20),
                Container(
                  width: 500,
                  child: RichText(
                    text: TextSpan(
                      text: "Encerrar a expedição ",
                      style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.amberAccent),
                      children: [
                        TextSpan(text: "ou ", style: Theme.of(context).textTheme.subtitle2),
                        TextSpan(text: "sair do jogo ", style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.amberAccent)),
                        TextSpan(text: "fará com que toda a ", style: Theme.of(context).textTheme.subtitle2),
                        TextSpan(text: "progressão ", style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.amberAccent)),
                        TextSpan(text: "da expedição ", style: Theme.of(context).textTheme.subtitle2),
                        TextSpan(text: "seja perdida. ", style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.amberAccent)),
                        TextSpan(text: "O que deseja fazer?", style: Theme.of(context).textTheme.subtitle2),
                      ],
                    ),
                  ),
                ),
                SizedBox(height: 20),
                Row(
                  mainAxisAlignment: MainAxisAlignment.center,
                  children: [
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).pop();
                        },
                        child: Image.asset(
                          "assets/images/botao_continuar.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          Navigator.of(context).pushReplacement(
                            MaterialPageRoute(builder: (context) => HomeView()),
                          );
                        },
                        child: Image.asset(
                          "assets/images/botao_encerrar.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                    Material(
                      color: Colors.transparent,
                      child: InkWell(
                        onTap: () {
                          SystemChannels.platform.invokeMethod('SystemNavigator.pop');
                        },
                        child: Image.asset(
                          "assets/images/botao_sair_jogo.png",
                          fit: BoxFit.cover,
                        ),
                      ),
                    )
                  ],
                ),
              ],
            ),
          )
        ],
      ),
    );
  }
}

import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/home_view.dart';

class VitoriaView extends StatefulWidget {
  final Game game;

  const VitoriaView({Key key, this.game}) : super(key: key);

  @override
  _VitoriaViewState createState() => _VitoriaViewState();
}

class _VitoriaViewState extends State<VitoriaView> {
  Widget build(BuildContext context) {
    widget.game.debug("Vitória");
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.blue.withOpacity(0.3),
          ),
          Container(child: Image.asset("assets/images/modal_vitoria.png", height: 350), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                Image.asset("assets/images/imagem_vitoria.png"),
                SizedBox(height: 20),
                Container(
                    width: 200, 
                    child: RichText(
                      text: TextSpan(
                        text: "A Exploração foi árdua, mas ",
                        style: Theme.of(context).textTheme.subtitle2,
                        children: [
                          TextSpan(text: 'vocês conseguiram!', style: Theme.of(context).textTheme.subtitle1.copyWith(fontWeight: FontWeight.bold, color: Colors.green)),
                        ],
                      ),
                    ),
                ),
                SizedBox(height: 20),
                Material(
                  color: Colors.transparent,
                  child: InkWell(
                    onTap: () {
                      Navigator.of(context).pushReplacement(
                        MaterialPageRoute(builder: (context) => HomeView()),
                      );
                    },
                    child: Image.asset(
                      "assets/images/botao_voltar_menu.png",
                      fit: BoxFit.cover,
                    ),
                  ),
                )
              ],
            ),
          )
        ],
      ),
    );
  }
}

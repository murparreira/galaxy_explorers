import 'package:flutter/material.dart';
import 'package:galaxy_explorers/app/models/Game.dart';
import 'package:galaxy_explorers/app/views/bau_view.dart';
import 'package:galaxy_explorers/app/views/tabuleiro_view.dart';

class LocalView extends StatefulWidget {
  final Game game;
  final Location location;

  const LocalView({Key key, this.game, this.location}) : super(key: key);

  @override
  _LocalViewState createState() => _LocalViewState();
}

class _LocalViewState extends State<LocalView> {
  @override
  Widget build(BuildContext context) {
    widget.game.debug("LOCAL");
    return Scaffold(
      body: Stack(
        children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset("assets/images/tabuleiro.png", fit: BoxFit.fill),
          ),
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            color: Colors.blue.withOpacity(0.3),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: Container(
                child: Image.asset("assets/images/modal_dificuldade.png", height: 350, fit: BoxFit.cover), width: MediaQuery.of(context).size.width, height: MediaQuery.of(context).size.height),
          ),
          Padding(
            padding: const EdgeInsets.all(16.0),
            child: Column(mainAxisAlignment: MainAxisAlignment.spaceBetween, children: [
              Center(child: Text(widget.location.name, style: TextStyle(fontFamily: "Goldman", fontSize: 30))),
              Expanded(
                child: ListView.separated(
                  shrinkWrap: true,
                  scrollDirection: Axis.horizontal,
                  itemCount: widget.location.monsterList.length,
                  itemBuilder: (BuildContext context, int index) {
                    return Column(mainAxisAlignment: MainAxisAlignment.center, children: [
                      InkWell(
                        onTap: () {
                          if (!widget.location.monsterList[index].defeated) {
                            setState(() {
                              widget.location.monsterList[index].defeated = true;
                            });
                          }
                        },
                        child: widget.location.monsterList[index].defeated
                            ? Image.asset("assets/images/" + widget.location.monsterList[index].imagem + "_completo.png")
                            : Image.asset("assets/images/" + widget.location.monsterList[index].imagem + ".png"),
                      ),
                      Text(widget.location.monsterList[index].name),
                    ]);
                  },
                  separatorBuilder: (BuildContext context, int index) => const Divider(),
                ),
              ),
              _buildBotoesLocal()
            ]),
          ),
        ],
      ),
    );
  }

  Widget _buildBotoesLocal() {
    if (widget.location.areAllMonstersDefeated()) {
      widget.location.hasLifeRisk = false;
      widget.game.removeLifeRiskFromPlayersOfThisLocation(widget.location.name);
      if (widget.location.hasBoss() && !widget.location.clearReward) {
        return Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              widget.location.clearReward = true;
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => BauView(game: widget.game)));
            },
            child: Image.asset(
              "assets/images/botao_recompensa.png",
              fit: BoxFit.cover,
            ),
          ),
        );
      } else {
        return Material(
          color: Colors.transparent,
          child: InkWell(
            onTap: () {
              widget.game.nextMove();
              Navigator.of(context).pushReplacement(MaterialPageRoute(builder: (context) => TabuleiroView(game: widget.game)));
            },
            child: Image.asset(
              "assets/images/botao_passar_vez.png",
              fit: BoxFit.cover,
            ),
          ),
        );
      }
    } else {
      return Material(
        color: Colors.transparent,
        child: InkWell(
          onTap: () {
            widget.location.hasLifeRisk = true;
            widget.game.currentPlayer.lifeRisk = true;
            widget.game.currentPlayer.locationOfLifeRisk = widget.location.name;
            widget.game.changeCurrentRoundPhase("passar_vez");
            Navigator.of(context).push(MaterialPageRoute(builder: (context) => TabuleiroView(game: widget.game)));
          },
          child: Image.asset(
            "assets/images/botao_pedir_ajuda.png",
            fit: BoxFit.cover,
          ),
        ),
      );
    }
  }
}

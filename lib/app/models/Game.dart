import 'dart:math';

class Game {

  Cenario cenario = new Cenario();
  int numberOfPlayers = 0;
  int numberOfRounds = 0;
  int currentPlayerNumber = 1;
  Player currentPlayer;
  int currentRound = 1;
  int numberOfRewards = 0;
  String currentRoundPhase = "tabuleiro";
  List<Player> playerList = new List<Player>();
  List<Player> playerListSelect = new List<Player>();
  List<Location> locationList = new List<Location>();

  fetchPlayerListSelect() {
    Player collin = new Player(1, "Collin", "collin", "collin_mini", "collin_selecionado");
    playerListSelect.add(collin);
    Player edgar = new Player(2, "Edgar", "edgar", "edgar_mini", "edgar_selecionado");
    playerListSelect.add(edgar);
    Player euranes = new Player(3, "Euranes", "euranes", "euranes_mini", "euranes_selecionado");
    playerListSelect.add(euranes);
    Player julia = new Player(4, "Julia", "julia", "julia_mini", "julia_selecionado");
    playerListSelect.add(julia);
    Player lucy = new Player(5, "Lucy", "lucy", "lucy_mini", "lucy_selecionado");
    playerListSelect.add(lucy);
    Player mauro = new Player(6, "Mauro", "mauro", "mauro_mini", "mauro_selecionado");
    playerListSelect.add(mauro);
    Player tom = new Player(7, "Tom", "tom", "tom_mini", "tom_selecionado");
    playerListSelect.add(tom);
    Player zivia = new Player(8, "Zivia", "zivia", "zivia_mini", "zivia_selecionado");
    playerListSelect.add(zivia);
    return playerListSelect;
  }

  fetchPlayersForGame() {
    playerList = new List<Player>();
    playerListSelect.forEach((player) {
      print(player.name);
      print(player.selected);
      if (player.selected) {
        playerList.add(player);
      }
    });
    playerList.first.current = true;
    resetCurrentPlayer();
  }

  fetchLocations() {
    locationList.clear();
    int bossCount = 0;
    Location hotel = new Location("Hotel", "hotel");
    hotel.fetchMonstersForLocation();
    if (hotel.hasBoss()) bossCount += 1;
    locationList.add(hotel);
    Location centroPesquisa = new Location("Centro de Pesquisa", "centro_de_pesquisa");
    centroPesquisa.fetchMonstersForLocation();
    if (centroPesquisa.hasBoss()) bossCount += 1;
    locationList.add(centroPesquisa);
    Location escola = new Location("Escola", "escola");
    escola.fetchMonstersForLocation();
    locationList.add(escola);
    if (escola.hasBoss()) bossCount += 1;
    Location torre = new Location("Torre", "torre");
    torre.fetchMonstersForLocation();
    locationList.add(torre);
    if (torre.hasBoss()) bossCount += 1;
    Location castelo = new Location("Castelo", "castelo");
    castelo.fetchMonstersForLocation();
    locationList.add(castelo);
    if (castelo.hasBoss()) bossCount += 1;
    Location ponte = new Location("Ponte", "ponte");
    ponte.fetchMonstersForLocation();
    locationList.add(ponte);
    if (ponte.hasBoss()) bossCount += 1;
    Location montanha = new Location("Montanha", "montanha");
    montanha.fetchMonstersForLocation();
    locationList.add(montanha);
    if (montanha.hasBoss()) bossCount += 1;
    Location gruta = new Location("Gruta", "gruta");
    gruta.fetchMonstersForLocation();
    locationList.add(gruta);
    if (gruta.hasBoss()) bossCount += 1;
    Location floresta = new Location("Floresta", "floresta");
    floresta.fetchMonstersForLocation();
    locationList.add(floresta);
    if (floresta.hasBoss()) bossCount += 1;
    Location templo = new Location("Templo", "templo");
    templo.fetchMonstersForLocation();
    locationList.add(templo);
    if (templo.hasBoss()) bossCount += 1;
    if (bossCount >= 3) {
      return;
    } else {
      fetchLocations();
    }
  }

  resetCurrentPlayer() {
    currentPlayer = playerList.first;
    currentPlayerNumber = 1;
  }

  incrementCurrentPlayer() {
    currentPlayer = playerList[currentPlayerNumber];
    currentPlayerNumber += 1;
  }

  incrementCurrentRound() {
    currentRound += 1;
  }

  incrementNumberOfRewards() {
    numberOfRewards += 1;
  }

  nextMove([tabuleiro="tabuleiro"]) {
    changeCurrentRoundPhase(tabuleiro);
    if (isLastPlayerOfRound()) {
      resetCurrentPlayer();
      incrementCurrentRound();
    } else {
      incrementCurrentPlayer();
    }
  }

  eliminatePlayerAndGoToNextMove([tabuleiro="tabuleiro"]) {
    changeCurrentRoundPhase(tabuleiro);
    playerList.removeAt(currentPlayerNumber - 1);
    if (isLastPlayerOfRound()) {
      numberOfPlayers -= 1;
      if (numberOfPlayers > 0) {
        resetCurrentPlayer();
      }
      incrementCurrentRound();
    } else {
      numberOfPlayers -= 1;
      currentPlayer = playerList[currentPlayerNumber - 1];
    }
  }

  changeCurrentRoundPhase(phase) {
    currentRoundPhase = phase;
  }

  bool isLastPlayerOfRound() {
    return currentPlayerNumber >= numberOfPlayers;
  }

  bool checkGameIsOver() {
    return numberOfRewards >= 3;
  }

  bool isPlayerInLifeRisk() {
    bool playerInLifeRisk = false;
    playerList.forEach((player) {
      if (player.lifeRisk) {
        playerInLifeRisk = true;
      }
    });
    return playerInLifeRisk;
  }

  List<Player> playersAtRisk() {
    List<Player> playersAtRisk = new List();
    playerList.forEach((player) {
      if (player.lifeRisk) {
        playersAtRisk.add(player);
      }
    });
    return playersAtRisk;
  }

  removeLifeRiskFromPlayersOfThisLocation(location) {
    playerList.forEach((player) {
      if (player.locationOfLifeRisk == location) {
        player.lifeRisk = false;
        player.locationOfLifeRisk = "";
      }
    });
  }

  debug(tela) {
    print("====================================");
    print(tela);
    print("numberOfRounds: " + numberOfRounds.toString());
    print("numberOfPlayers: " + numberOfPlayers.toString());
    if (currentPlayer != null) {
      print("currentPlayer: " + currentPlayer.name.toString());
    }
    print("currentPlayerNumber: " + currentPlayerNumber.toString());
    print("currentRoundPhase: " + currentRoundPhase.toString());
    print("currentRound: " + currentRound.toString());
    print("------------------------------------");
    print("locationList: ");
    locationList.forEach((location) {
      print("location: " + location.name + " hasLifeRisk: " + location.hasLifeRisk.toString());
      location.monsterList.forEach((monster) {
        print("monster: " + monster.name + " defeated: " + monster.defeated.toString() + " isBoss: " + monster.isBoss.toString());
      });
    });
    print("------------------------------------");
    print("playerList: ");
    playerList.forEach((player) {
      print("player: " + player.name + " number: " + player.number.toString() + " lifeRisk: " + player.lifeRisk.toString() + " locationOfLifeRisk: " + player.locationOfLifeRisk.toString());
    });
    print("------------------------------------");
    print("====================================");
  }
}

class Cenario {
  String nome;
  String dificuldade;
  String descricao;
  String imagem;
}

class Player {
  int number;
  String name;
  bool lifeRisk = false;
  String locationOfLifeRisk = "";
  bool current = false;
  bool selected = false;
  String imagem;
  String imagemMini;
  String imagemSelected;

  Player(this.number, this.name, this.imagem, this.imagemMini, this.imagemSelected);
}

class Location {
  final String name;
  final String imagem;
  bool clearReward = false;
  bool hasLifeRisk = false;
  List<Monster> monsterList;

  Location(this.name, this.imagem);

  fetchMonstersForLocation() {
    Random random = new Random();
    int randomNumberChance = random.nextInt(100);
    int numberOfMonsters = 0;
    bool isBossPresent = false;
    if (randomNumberChance >= 75) {
      isBossPresent = true;
    }
    if (randomNumberChance >= 85) {
      numberOfMonsters = 3;
    } else if (randomNumberChance >= 70) {
      numberOfMonsters = 2;
    } else {
      numberOfMonsters = 1;
    }
    monsterList = new List<Monster>();
    for (int i = 0; i < numberOfMonsters; i++) {
      monsterList.add(Monster("Monstro: " + random.nextInt(1000).toString(), false, false, "monstro", "moldura_monstro"));
    }
    if (isBossPresent) {
      monsterList.add(Monster("Boss: " + random.nextInt(1000).toString(), false, true, "boss", "moldura_boss"));
    }
  }

  bool areAllMonstersDefeated() {
    bool allDefeated = true;
    monsterList.forEach((monster) {
      if (!monster.defeated) {
        allDefeated = false;
      }
    });
    return allDefeated;
  }

  bool hasBoss() {
    bool hasBoss = false;
    monsterList.forEach((monster) {
      if (monster.isBoss) {
        hasBoss = true;
      }
    });
    return hasBoss;
  }

}

class Monster {
  final String name;
  bool defeated;
  final bool isBoss;
  final String imagem;
  final String imagemMoldura;

  Monster(this.name, this.defeated, this.isBoss, this.imagem, this.imagemMoldura);
}
